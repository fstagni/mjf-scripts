Name: mjf-db12
Version: %(echo ${MJF_VERSION:-0.0})
Release: 1
BuildArch: noarch
Summary: DIRAC Benchmark (DB12) for Machine/Job Features
License: BSD
Group: System Environment/Daemons
Source: mjf-scripts.tgz
Vendor: GridPP
Packager: Andrew McNab <Andrew.McNab@cern.ch>

%description
Run at boot time to produce DIRAC Benchmark (DB12) and write to /etc/db12 ready for MJF

%prep

%setup -n mjf-scripts

%build

%install
make db12-install

%post
chkconfig db12 on

%preun
if [ "$1" = "0" ] ; then
  # if uninstallation rather than upgrade then stop
  chkconfig db12 off
fi

%files
/etc/db12
/etc/rc.d/init.d/*
/usr/sbin/*
